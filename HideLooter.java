import org.rspeer.runetek.adapter.scene.Pickable;
import org.rspeer.runetek.adapter.scene.Player;
import org.rspeer.runetek.adapter.scene.SceneObject;
import org.rspeer.runetek.api.commons.StopWatch;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.component.Bank;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.api.movement.Movement;
import org.rspeer.runetek.api.movement.position.Area;
import org.rspeer.runetek.api.movement.position.Position;
import org.rspeer.runetek.api.scene.Pickables;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.runetek.api.scene.SceneObjects;
import org.rspeer.runetek.event.types.RenderEvent;
import org.rspeer.script.Script;
import org.rspeer.script.ScriptMeta;
import org.rspeer.script.ScriptCategory;
import org.rspeer.ui.Log;
import org.rspeer.runetek.event.listeners.RenderListener;
import java.awt.*;


@ScriptMeta(name = "Logan's HideLooter",  desc = "Picks up cowhides and banks them at Lumbridge Bank", developer = "Logan", category = ScriptCategory.MONEY_MAKING)
public class HideLooter extends Script implements RenderListener {


    public static final Area COW_AREA =  Area.rectangular(3241, 3298, 3265, 3255);
    public static final Position GATE_POSITION = new Position(3254, 3266, 0); 

    
    private StopWatch stopWatch = StopWatch.start();

    private long totalHides; 
    private long previousHides; 

    private void bank() {
        if (Bank.isOpen()) {
            Bank.depositInventory();
        } else {
            final SceneObject Bank_Booth = SceneObjects.getNearest("Bank booth");
            if (Bank_Booth != null) {
                Bank_Booth.interact(a -> true);
            }
        }
    }

    @Override
    public void onStart() {
        previousHides = Inventory.getCount(1739);
    }

    @Override
    public int loop() {
        Player local = Players.getLocal();
        int floorLevel = local.getFloorLevel();
        Area BANK_AREA =  Area.rectangular(3200, 3208, 3215, 3228, floorLevel);
        long inventoryCount = Inventory.getCount(1739); 

        if(inventoryCount > previousHides) {
            totalHides += (inventoryCount - previousHides);
        }
        previousHides = inventoryCount;


        if (Movement.getRunEnergy() > 20 && !Movement.isRunEnabled()) {
            Movement.toggleRun(true);

        }

        if (Inventory.isFull()) { 
            if (BANK_AREA.contains(local)) {
                if (((local.getFloorLevel() == 0)) || (local.getFloorLevel() == 1)) { 
                    final SceneObject staircase = SceneObjects.getNearest(sceneObject -> sceneObject.getName().equalsIgnoreCase("Staircase")); 
                    if (staircase != null) {
                        Log.info("Attempting to walk upstairs..");
                        staircase.interact("Climb-up");
                    }
                } else if (local.getFloorLevel() == 2) {
                    Log.info("Attempting to bank inventory..");
                    bank();
                }
            } else { 
                if (COW_AREA.contains(local))
                {
                    if (local.distance(COW_AREA.getCenter()) > 30){
                        Movement.setWalkFlag(COW_AREA.getCenter());
                    }
                    final SceneObject gate = SceneObjects.getNearest(1558);
                    if (gate != null && gate.containsAction("Open") && COW_AREA.contains(gate)) {
                        Movement.setWalkFlag(gate);
                        Log.info("Opening gate");
                        Time.sleep(300);
                        gate.interact("Open");
                    }
                    else{
                        Log.info("Walking to bank..");
                        Movement.setWalkFlag(BANK_AREA.getCenter());
                    }

                }
                else {
                    Log.info("Walking to bank..");
                    Movement.setWalkFlag(BANK_AREA.getCenter());
                }

            }
        } else { 
            if (local.getFloorLevel() == 0) {

                if (COW_AREA.contains(local)) {
                    final SceneObject gate = SceneObjects.getNearest(1558);
                    if (gate != null && gate.containsAction("Open") && COW_AREA.contains(gate)) {
                        Movement.setWalkFlag(gate);
                        Log.info("Opening gate");
                        Time.sleep(300);
                        gate.interact("Open");
                    }
                    else {
                        if (local.getAnimation() == -1 && !local.isMoving()) {
                            Log.info("Looking for hides..");
                            final Pickable cowhide = Pickables.getNearest("Cowhide");
                            if (cowhide != null) {
                                if (Movement.isWalkable(cowhide)) {
                                    cowhide.interact("Take");
                                }
                            } else {
                                Log.info("12");
                                Movement.setWalkFlag(GATE_POSITION);
                            }
                        }
                    }
                } else {
                    Log.info("Walking to cows..");
                    Movement.setWalkFlag(COW_AREA.getCenter());
                }
            } else {
                if (BANK_AREA.contains(local)) {
                    if (local.getFloorLevel() == 1) {
                        Log.info("Attempting to walk downstairs..");
                        final SceneObject staircase = SceneObjects.getNearest(sceneObject -> sceneObject.getName().equalsIgnoreCase("Staircase")); 
                        if (staircase != null) {
                            staircase.interact("Climb-Down");
                        }
                    } else if (local.getFloorLevel() == 2) {
                        Log.info("Attempting to walk downstairs..");
                        final SceneObject staircase = SceneObjects.getNearest(sceneObject -> sceneObject.getName().equalsIgnoreCase("Staircase"));
                        if (staircase != null){
                            staircase.interact("Climb-Down");
                        }

                    }
                }
            }
        }
        return 600; 
    }

    @Override
    public void onStop() {
        Log.info("Script Stopping");
    }

    @Override
    public void notify(RenderEvent renderEvent) {
        final Font font1 = new Font("Arial  Black", 0, 15);
        final Color color1 = new Color(0, 0, 0, 100);
        final Color color2 = new Color(0, 0, 0);
        Graphics g = renderEvent.getSource();
        g.setColor(color1);
        g.fillRect(8, 25, 170, 50);
        g.setColor(color2);
        g.drawRect(8, 25, 170, 50);
        g.setFont(font1);
        g.setColor(Color.RED);
        g.drawString("Running for: " + stopWatch.toElapsedString(), 20, 50); 
        g.drawString("Total Hides: " + totalHides, 20, 64);
    }
}
